# deprecated

import requests
import sys

try:
    download_url = sys.argv[1]
except IndexError:
    print("Missing download url")
    sys.exit(1)

if download_url is None:
    print("download url not found", file=sys.stderr)
    sys.exit(1)

print("downloading shape zip ...")
res = requests.get(download_url, stream=True)

if res.status_code != 200:
    print("invalid status code", file=sys.stderr)
    sys.exit(1)

with open("civici.zip", "wb") as dst:
    for chunk in res.iter_content(2048):
        dst.write(chunk)

sys.exit(0)
