#!/usr/bin/python3

import fiona
import xml.etree.cElementTree as et
import sys

try:
    filename = sys.argv[1]
except IndexError:
    print("missing filename.", file=sys.stderr)
    sys.exit(1)

osm = et.Element("osm")

with fiona.open(filename) as fp:
    for i, item in enumerate(fp, start=1):

        lat, lon = item.geometry.coordinates
        ## crea il nodo
        node = et.SubElement(osm,
                             "node",
                             visible="true",
                             id=str(-i),
                             lat=str(lat),
                             lon=str(lon))
        try:
            ## numero civico
            et.SubElement(node, "tag", k="addr:housenumber", v=item.properties.get("NUMESP_CIV", ")"))

            ## strada
            et.SubElement(node, "tag", k="addr:street", v=item.properties.get("TOPONIMO").title())

            ## codice postale
            et.SubElement(node, "tag", k="addr:postcode", v=str(int(item.properties.get("CAP"))))

            ## città   ATTENZIONE la proprietà 'CAB_CDS' potrebbe non esistere
            et.SubElement(node, "tag", k="addr:city", v=(item.properties.get("CAB_CDS") or "?").title())

        except:
            ## c'è un errore non gestito nel codice
            ## stampo gli attributi
            print("PROPERTIES:")
            for key, value in item.properties.items():
                print("  %-12s %s" % (key, value))
            print("\nGEOMETRY:")
            for key, value in item.geometry.items():
                print("  %-12s %s" % (key, value))
            raise

tree = et.ElementTree(osm)
et.indent(tree)
tree.write("output.xml")

sys.exit(0)
